import React, {useState, useEffect} from 'react'
import axios from 'axios'

const ShowData = () => {
    const [ data, setData ] = useState([])
    const [totalPurchased, setTotalPurchased] = useState(0)
    const [totalAdditional, setTotalAdditional] = useState(0)

    useEffect(() => {      
        axios
        .get("https://jsonkeeper.com/b/356L")
        .then(res=> {
            let data = res.data.data.purchased_services
            let tempArr = []
            let totalPurchased = 0
            let totalAdditional = 0
            for(let i = 0; i < data.length; i++){
                let purchasedService = data[i].purchased_office_template.purchased_office_services
                let tempObj = {}
                let tempPurchasedArr = []
                let tempAdditionalArr = []
                for(let j = 0; j < purchasedService.length; j++){
                    tempObj.name = data[i].name
                    if(purchasedService[j].service_selected !== null){
                        tempPurchasedArr.push(purchasedService[j])
                        totalPurchased += parseInt(purchasedService[j].price)

                    }
                    else if(purchasedService[j].service_selected === null){
                        tempAdditionalArr.push(purchasedService[j])
                        totalAdditional += parseInt(purchasedService[j].price)
                    }
                }
                    tempObj.purchased = tempPurchasedArr
                    tempObj.additional = tempAdditionalArr
                    tempArr.push(tempObj)
            }
            setTotalPurchased(totalPurchased)
            setTotalAdditional(totalAdditional)
            setData(tempArr)
        })
    }, [])

    console.log(data);

    return (
        <div>
            <h1>Purchased Service</h1>
            {
                data && data.length > 0 && data.map((item, index) => {
                    if(item.purchased.length > 0){
                        return(
                            <div key={index}>
                                {item.name}
                                <div>
                                  {
                                      item.purchased.map(pItem =>{
                                          return(
                                              <div key={pItem.id}>
                                                  <p>{pItem.name}</p>
                                                  <p>{pItem.price}</p>
                                                  <img src={pItem.image} alt={pItem.name}/>
                                             </div>
                                          )
                                      })
                                  }
                                </div>
                            </div>
                        )
                    }
                    else{
                        return null
                    }
                })
               
            }
<div>
   Total =  {totalPurchased}
</div>
        <h1>Additional Services</h1>
            {
                data && data.length > 0 && data.map((item, index) => {
                    if(item.additional.length > 0){
                        return(
                            <div key={index}>
                                {item.name}
                                <div>
                                  {
                                      item.additional.map(aItem =>{
                                          return(
                                              <div key={aItem.id}>
                                                    <p>{aItem.name}</p>
                                                    <p>{aItem.price}</p>
                                                    <img src={aItem.image} alt={aItem.name}/>
                                             </div>
                                          )
                                      })
                                  }
                                </div>
                            </div>
                        )
                    }
                    else{
                        return null
                    }
                })
            }
          Total : {totalAdditional}
        </div>
    )
}

export default ShowData